#ifndef BLADE_GENERIC_BENDER_CONFIG_H
#define BLADE_GENERIC_BENDER_CONFIG_H


namespace BLADE_GENERIC_BENDER
{
	static const string NAME								="Blade Generic Bender";

	namespace BENDER1
	{
		static const string DESCRIPTION						= "Bender 1";
		static const double VAL								= 0.0;
		static const double MINIMAL_VAL						= -20.0;
		static const double MAXIMAL_VAL						= 20.0;
	}
	
	namespace BENDER2
	{
		static const string DESCRIPTION						= "Bender 2";
		static const double VAL								= 0.0;
		static const double MINIMAL_VAL						= -20.0;
		static const double MAXIMAL_VAL						= 20.0;
	}

	namespace FUNCTIONING_MODE
	{

		namespace INDEPENDANT
		{
				static const string NAME					= "Independant";
				static const int	INDEX					= 1;
		}
		namespace SYMMETRICAL
		{
				static const string NAME					= "Symmetrical";
				static const int	INDEX					= 2;
		}
		namespace ANTISYMMETRICAL
		{
				static const string NAME					= "Anti-Symmetrical";
				static const int	INDEX					= 3;
		}
	}


	namespace DEVICE_INIT_PROPERTIES
	{
			static const string COMMAND_STOP_NAME			= "stop";
			static const string COMMAND_STATE_NAME			= "State";
			static const string ATTRIBUTE_POSITION_NAME		= "position";
			static const string BENDER_1_MOTOR_NAME			= "tmp/ode/bender1";
			static const string BENDER_2_MOTOR_NAME			= "tmp/ode/bender2";
	}
	

}



#endif

